# inChurch Recruitment Process 2020 - iOS Developer

Nessa parte do processo de recrutamento voc� desenvolver� uma aplica��o iOS. O desafio deve ser desenvolvido em Swift e utilizando libs conhecidas de mercado. A aplica��o ser� um cat�logo dos filmes populares, utilizando a [API](https://developers.themoviedb.org/3/getting-started/introduction) do [TheMovieDB](https://www.themoviedb.org/).

* * *

## Bem vindos ao meu teste

+### O que a aplica��o faz?
Criei um app para iOS utilizando da linguagem Swift que consome o JSON exposto pela API de busca e apresenta as informa��es em cards em uma UICollectionView. Cada card apresenta um filme com: nome, capa e um bot�o para favorita-lo.

Na aba dos famoritos � poss�vel utilizar um filtro que se apresenta de 3 formas: nome do filme, generos e data de lan�amento, dessa forma o usu�rio pode escolher como categorizar sua busca e achar seu filme da forma que achar mais eficiente.

+### Pods
Foram utilizados 3 pods nesse projeto:
O primeiro foi o Alamofire, para se ter um netowrk layer mais robusto, substituindo o uso do URL session e se adequar � pr�ticas do mercado. 
O segundo foi o Kingfisher, para a realiz��o do loading das imagens, que permite facilmente a realiza��o das tarefas sem a utiliza��o de muitas linhas de c�digo, deixando assim o mesmo mais limpo.
E por �ltimo MarqueeLabel: que permite a cria��o de labels que se movimentam, permidindo assim driblar o limite de espa�o das c�lulas da primeira tela de feed.
No projeto ainda existe o Snapkit que n�o foi implmentado no c�digo devido a falta de experiencia com o mesmo minha exp�riencia com a forma nativa de cria��o de constraints.



+###  TESTES

A aplica��o cont�m testes automatizados para o network layer podendo avaliar o resultado dos requests realizado no Movie DB assim como o tempo em que essa request leva para ser feita.
	
+###  UI | UX
* Toda a identidade visual do app foi inspirada em apps de busca de filme, as escolhas das cores foi inspirada em um tom mais s�rio e que n�o fosse cansativo aos olhos, os tons de azul, segundo a teoria das cores, invocam um sentimento de calma, o que ao meu ver se encaixa com a busca de filmes

*O app foi testado com usu�rios pr�ximos e volunt�rios para testar o fluxo de telas e a clareza dos bot�es e outros clicaveis.

-**Instala��o
    *Para instalar o projeto deve-se abrir o terminal e degitar o seguintes comandos:

*git clone <pasta que deseja clonar o reposit�rio>
    -**Depois, para clonar o reposit�rio

*git clone <url do reposit�rio>
    -**Depois, para instalar os pods referentes ao banco de dados:

*pod install
    -**E finalmente, para abrir o projeto deve-se abrir o .xcworkspace que est� contido na pasta.

+###Aparelhos para Rodar a Aplica��o
    *O app foi desenvolvido para rodar em IPhones com Deployment Target 13.2. Pode-se rodar em um simulador do Xcode ou em algum aparelho pr�prio.

*Al�m disso, o app possui Auto Layout (Constraints) para aparelhos desde do IPhone 11 Pro Max at� o IPhone 7, s� n�o cobrindo IPhones SE e 4, j� que estes, n�o possuem Deployment Target compat�veis.


